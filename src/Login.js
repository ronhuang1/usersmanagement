import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

const LoginForm = ({ setToken }) => (
  <Formik
    initialValues={{ email: '', password: '' }}
    validationSchema={Yup.object().shape({
      email: Yup.string().email().required('No email provided.'),
      password: Yup.string().required('No password provided.'),
    })}
    onSubmit={async (values, { setSubmitting }) => {
      setTimeout(() => {
        console.log('Logging in', values);
        setSubmitting(false);
      }, 500);
      const response = await fetch('api/v2/users/tokens', {
        method: 'POST',
        body: JSON.stringify(values),
      });
      const data = await response.headers.map.authorization;
      setToken(data);
    }}
  >
    {(props) => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit
      } = props;

      return (
        <form onSubmit={handleSubmit}>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            name="email"
            type="text"
            placeholder="Enter your email"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
            className={errors.email && touched.email && 'error'}
          />
          {errors.email && touched.email && <div className="input-feedback">{errors.email}</div>}

          <br />

          <label htmlFor="password">Password</label>
          <input
            id="password"
            name="password"
            type="password"
            placeholder="Enter your password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
            className={errors.password && touched.password && 'error'}
          />
          {errors.password && touched.password && (
            <div className="input-feedback">{errors.password}</div>
          )}

          <br />

          <button type="submit" disabled={isSubmitting}>
            Login
          </button>
        </form>
      );
    }}
  </Formik>
);

export default LoginForm;
