import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginForm from './Login';
import CreatOrEdit from './Table/CreatOrEdit';
import UserDetail from './Table/UserDetail';
import UserIndex from './Table/UserIndex';
import useToken from './useToken';

const App = () => {
  const { token, setToken } = useToken();
  if (!token) {
    return <LoginForm setToken={setToken} />;
  }
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <UserIndex />
          </Route>
          <Route path="/details/:id">
            <UserDetail />
          </Route>
          <Route path="/edit/:id">
            <CreatOrEdit />
          </Route>
          <Route path="/creat">
            <CreatOrEdit />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
