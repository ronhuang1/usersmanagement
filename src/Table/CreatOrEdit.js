import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import useToken from '../useToken';
import { useHistory } from 'react-router-dom';

const CreatOrEdit = () => {
  const { token, setToken } = useToken();
  const history = useHistory();
  return (
    <Formik
      initialValues={{
        email: '',
        first_name: '',
        last_name: '',
        jobs_count: '',
        active: '',
        slack_username: '',
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email().required('No email provided.'),
        first_name: Yup.string().required('No first name provided.'),
        last_name: Yup.string().required('No last name provided.'),
        jobs_count: Yup.number().positive().integer().required('No jobs count provided.'),
        active: Yup.boolean().required('No active status provided.'),
        slack_username: Yup.string().required('No slack username provided.'),
      })}
      onSubmit={async (values, { setSubmitting }) => {
        setTimeout(() => {
          console.log('Submitting', values);
          setSubmitting(false);
        }, 500);

        if (window.location.href.includes('edit')) {
          const loc = `api/v2/users/${window.location.href.split('/').pop()}`;
          fetch(loc, {
            headers: {
              Authorization: token,
            },
            method: 'PATCH',
            body: JSON.stringify(values),
          });
          history.push('/');
          return;
        }
        fetch('api/v2/users', {
          headers: {
            Authorization: token,
          },
          method: 'POST',
          body: JSON.stringify(values),
        });
        history.push('/');
      }}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;

        return (
          <form onSubmit={handleSubmit}>
            <label htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              type="text"
              placeholder="Enter your email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.email && touched.email && 'error'}
            />
            {errors.email && touched.email && <div className="input-feedback">{errors.email}</div>}

            <br />

            <label htmlFor="first_name">First Name</label>
            <input
              id="first_name"
              name="first_name"
              type="text"
              placeholder="Enter your first name"
              value={values.first_name}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.first_name && touched.first_name && 'error'}
            />
            {errors.first_name && touched.first_name && (
              <div className="input-feedback">{errors.first_name}</div>
            )}

            <br />

            <label htmlFor="last_name">Last Name</label>
            <input
              id="last_name"
              name="last_name"
              type="text"
              placeholder="Enter your last name"
              value={values.last_name}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.last_name && touched.last_name && 'error'}
            />
            {errors.last_name && touched.last_name && (
              <div className="input-feedback">{errors.last_name}</div>
            )}

            <br />

            <label htmlFor="jobs_count">Jobs Count</label>
            <input
              id="jobs_count"
              name="jobs_count"
              type="text"
              placeholder="Enter your jobs count"
              value={values.jobs_count}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.jobs_count && touched.jobs_count && 'error'}
            />
            {errors.jobs_count && touched.jobs_count && (
              <div className="input-feedback">{errors.jobs_count}</div>
            )}

            <br />

            <label htmlFor="active">Active</label>
            <input
              id="active"
              name="active"
              type="bollean"
              placeholder="Enter your active status"
              value={values.active}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.active && touched.active && 'error'}
            />
            {errors.active && touched.active && (
              <div className="input-feedback">{errors.active}</div>
            )}

            <br />

            <label htmlFor="slack_username">Slack Username</label>
            <input
              id="slack_username"
              name="slack_username"
              type="text"
              placeholder="Enter your slack username"
              value={values.slack_username}
              onChange={handleChange}
              onBlur={handleBlur}
              className={errors.slack_username && touched.slack_username && 'error'}
            />
            {errors.slack_username && touched.slack_username && (
              <div className="input-feedback">{errors.slack_username}</div>
            )}

            <br />

            <button type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </form>
        );
      }}
    </Formik>
  );
};

export default CreatOrEdit;
