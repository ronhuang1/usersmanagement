import React, { useEffect, useState } from 'react';
import { useTable, usePagination, useFilters } from 'react-table';
import { useHistory } from 'react-router-dom';
import useToken from '../useToken';
import UserDetail from './UserDetail';

const columnFilter = ({ column }) => {
  const { filterValue, setFilter } = column;
  return (
    <span>
      Search: <input value={filterValue || ''} onChange={(e) => setFilter(e.target.value)} />
    </span>
  );
};

function SelectColumnFilter({ column: { filterValue, setFilter, preFilteredRows, id } }) {
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = React.useMemo(() => {
    const options = new Set();
    preFilteredRows.forEach((row) => {
      options.add(row.values[id]);
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  // Render a multi-select box
  return (
    <select
      value={filterValue}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
    >
      <option value="">All</option>
      {options.map((option, i) => (
        <option key={i} value={option}>
          {option}
        </option>
      ))}
    </select>
  );
}

function Table({ columns, data }) {
  const defaultColumn = React.useMemo(
    () => ({
      Filter: columnFilter,
    }),
    []
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: { pageIndex: 0 },
    },
    useFilters,
    usePagination
  );

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>
                  {column.render('Header')}
                  <div>{column.canFilter ? column.render('Filter') : null}</div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span>
          | Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: '100px' }}
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[3, 5, 10, 20].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

function UserIndex() {
  const [data, setData] = useState({ users: [] });
  const { token, setToken } = useToken();
  const [changeFlag, setFlag] = useState(0);
  const history = useHistory();

  const columns = React.useMemo(
    () => [
      {
        Header: 'ID',
        accessor: 'id',
        disableFilters: true,
      },
      {
        Header: 'Email',
        accessor: 'email',
        Filter: columnFilter,
      },
      {
        Header: 'Jobs Count',
        accessor: 'jobs_count',
        disableFilters: true,
      },
      {
        Header: 'Active',
        Filter: SelectColumnFilter,
        accessor: (d) => d.active.toString(),
      },
      {
        id: 'view',
        accessor: 'id',
        disableFilters: true,
        Cell: ({ value }) => (
          <button onClick={() => history.push('/details/' + value)}>View</button>
        ),
      },
      {
        id: 'edit',
        accessor: 'id',
        disableFilters: true,
        Cell: ({ value }) => <button onClick={() => history.push('/edit/' + value)}>Edit</button>
      },
      {
        id: 'delete',
        accessor: 'id',
        disableFilters: true,
        Cell: ({ value }) => <button onClick={() => {
          fetch('api/v2/users/' + value, {
            method: 'DELETE',
            headers: {
              Authorization: token,
            }
          });
          setFlag(changeFlag+1)
        }}>Delete</button>,
      },
    ],
    [data]
  );

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch('api/v2/users', {
        headers: {
          Authorization: token,
        },
      });
      const usersObj = await result.json();
      setData(usersObj);
    };

    fetchData();
  }, [changeFlag]);

  function HandleLogout() {
    console.log(0)
    fetch('api/v2/users/tokens', {
      method: 'DELETE',
    });
    console.log(1)
    localStorage.removeItem('token');
    window.location.reload();
  }

  return (
    <div>
      <Table columns={columns} data={data.users} />
      <br />
      <button onClick={() => history.push('/creat')}>Creat New User</button>
      <br />
      <br />
      <br />
      <button onClick={HandleLogout}>Logout</button>
    </div>
  );
}

export default UserIndex;
