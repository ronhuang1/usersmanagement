import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import useToken from '../useToken';

function UserDetail() {
  const loc = window.location.href.split('/').pop();
  const { token, setToken } = useToken();
  const [obj, setObj] = useState({});
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`/api/v2/users/${loc}`, {
        headers: {
          Authorization: token,
        },
      });
      const data = await response.json();
      setObj(data.users);
    };
    fetchData();
  }, []);
  return (
    <div>
      <h1>UserDetail</h1>
      <p>
        {`ID: ${obj.id}`}
        <br />
        {`Email: ${obj.email}`}
        <br />
        {`First Name : ${obj.first_name}`}
        <br />
        {`Last Name : ${obj.last_name}`}
        <br />
        {`Jobs Count : ${obj.jobs_count}`}
        <br />
        {`Active : ${obj.active}`}
        <br />
        {`Slack Username : ${obj.slack_username}`}
        <br />
      </p>
      <button onClick={() => history.push('/')}>Return</button>
    </div>
  );
}

export default UserDetail;
